/**
 * 
 */
package edu.birzeit.swen6301.Test;

import static org.junit.jupiter.api.Assertions.*;
/*
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;*/
import java.io.IOException;


import java.net.URL;
import java.text.ParseException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import edu.birzeit.swen6301.BL.Manifest;
import edu.birzeit.swen6301.BL.Reader;
import edu.birzeit.swen6301.BL.Segment;
import junit.framework.Assert;

/**
 * @author NaNo Group
 *
 */
class JUnitTesting {

	Segment segment = new Segment();
	Reader mirror = new Reader();

	String readActualFile(String urlPath) {
		URL url;
		String str_file = null;
		Scanner s;
		try {
			url = new URL(urlPath);
			s = new Scanner(url.openStream());
			while (s.hasNext()) {
				str_file = str_file + s.nextLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			str_file = new String(mirror.download(urlPath));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str_file;
	}

	@Test
	public void checkValidURLSegment() throws IOException, Exception {

		Manifest manifest = new Manifest();
		String resultFileContent = "";
		String expectedFileContent = "";

		byte[] fileBytes = null;
		try {
			manifest.loadAndBind("http://52.74.234.162/MDFiles/text_manifest.txt.segments");
			fileBytes = manifest.downloadAllNested();
			resultFileContent = new String(fileBytes);
			expectedFileContent = readActualFile("http://52.74.234.162/MDFiles/text.txt");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

/*		System.out.println("Expected is:\n" + expectedFileContent);
		System.out.println("Resulted is:\n" + resultFileContent);
		System.out.println("Comparison Result is: " + expectedFileContent.compareTo(resultFileContent));
		File f1 = new File("C:\\Users\\ahmad.zuhd\\eclipse-workspace\\mainrepo\\NanoGroupMPDProject\\resultfile.txt");
		 FileOutputStream fos = new FileOutputStream(f1);
         OutputStreamWriter osw = new OutputStreamWriter(fos);    
         Writer w = new BufferedWriter(osw);
         w.write(resultFileContent);
         w.close();
 */
		Assert.assertEquals(expectedFileContent, resultFileContent);
	}

}
