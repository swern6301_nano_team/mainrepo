package edu.birzeit.swen6301.Test;

import java.io.FileOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import edu.birzeit.swen6301.BL.*;

/**
 * @author NaNo Group
 *
 */
public class DebuggClass {

	public static void main(String[] args) {
		DebuggClass debugger = new DebuggClass();
		debugger.debugReader();
		debugger.debugSegment();
		debugger.debugManifest();
	}

	private void debugReader() {
		Reader reader = new Reader();
		try {
			byte[] fileBytes = reader
					.download("http://52.74.234.162/MDFiles/reader.txt");
			new FileOutputStream("testingOutput/test_reader.txt").write(fileBytes);
			reader.closeStream();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error in Reader Class: " + e);
		}
	}

	private void debugSegment() {
		Segment segment = new Segment(null);
		try {
			Path p = FileSystems.getDefault().getPath("", "input/segment.txt");
			segment.bind(new String(Files.readAllBytes(p)));
			byte[] fileBytes = segment.download();
			new FileOutputStream("testingOutput/test_segment.txt").write(fileBytes);
			segment.closeStream();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error in Segment Class: " + e);
		}
	}

	private void debugManifest() {
		Manifest manifest = new Manifest();
		try {
			manifest.loadAndBind(
					"http://52.74.234.162/MDFiles/text_manifest.txt.segments");
			byte[] fileBytes = manifest.downloadAllNested();
			new FileOutputStream("testingOutput/text_manifest.txt").write(fileBytes);
			manifest.closeStream();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error in Manifest Class (txt): " + e);
		}
	}
}
