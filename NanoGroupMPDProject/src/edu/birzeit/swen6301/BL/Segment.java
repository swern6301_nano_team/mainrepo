/**
 * 
 */
package edu.birzeit.swen6301.BL;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author NaNo Group
 *
 */
public class Segment implements IPart, Comparator<Object>, Comparable<Object> {

	private int rank;
	private boolean endLess;
	private List<Reader> Readers;
	private byte[] bytes;
	private Reader openedReader;
	private IPart parentPart;
	private IPart nextPart;
	private IPart previousPart;

	public Segment() {
	}

	public Segment(IPart parent) {
		this.setParentPart(parent);
	}

	public Segment(String segmentStr, IPart parent) throws IOException {
		this(parent);
		this.bind(segmentStr);
	}

	public Segment(IPart parent, String segmentStr, boolean mainManifestContents) throws IOException {
		this(parent);
		this.bind(segmentStr, mainManifestContents);
	}

	public Segment(IPart parent, String[] lines) throws IOException {
		this(parent);
		this.bind(lines);
	}

	public int getRank() {
		return rank;
	}

	public Reader getOpenedReader() {
		return openedReader;
	}

	public void setOpenedReader(Reader opendReader) {
		this.openedReader = opendReader;
	}

	public void setParentPart(IPart parentPart) {
		this.parentPart = parentPart;
	}

	@Override
	public void setNextPart(IPart nextPart) {
		this.nextPart = nextPart;
	}

	@Override
	public void setPreviousPart(IPart previousPart) {
		this.previousPart = previousPart;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Manifest getManifest() {
		if (this.getOpenedReader() == null)
			return null;
		else
			return this.getOpenedReader().getManifest();
	}

	public List<Reader> getReaders() {
		return Readers;
	}

	public void setReaders(List<Reader> Readers) {
		this.Readers = Readers;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	@Override
	public String getUrl() {
		if (this.getOpenedReader() == null)
			return null;
		else
			return this.getOpenedReader().getUrl();
	}

	public void addReader(Reader Reader) {
		if (Readers == null)
			Readers = new ArrayList<Reader>();

		Readers.add(Reader);
	}

	@Override
	public String getMimeType() {
		if (this.getOpenedReader() == null)
			return null;
		else
			return this.getOpenedReader().getMimeType();
	}

	public void bind(String[] segmentUrls) throws IOException {
		this.bind(segmentUrls, false);
	}

	public void bind(String[] segmentUrls, boolean mainManifestContents) throws IOException {
		// Segments' lines loop.
		for (int i = 0; i < segmentUrls.length; i++) {
			try {
				Reader Reader = new Reader(segmentUrls[i]);
				Reader.setParentPart(this);
				this.addReader(Reader);
				if (this.getOpenedReader() == null) {
					Reader.setPreviousPart(this);
					Reader.openStream(mainManifestContents);
					this.setOpenedReader(Reader);
				}
			} catch (IOException e) {
				String msg = "Reader stream error: (" + segmentUrls[i] + ") opening is failed, " + e.getMessage();
				System.err.println(msg);
				if (mainManifestContents)
					throw new IOException();
			}
		}
	}

	public void bind(String segmentStr) throws IOException {
		this.bind(segmentStr, false);
	}

	public void bind(String segmentStr, boolean mainManifestContents) throws IOException {
		String segmentUrls[] = segmentStr.split("\\r?\\n"); // Split string of segment into array of string.
		this.bind(segmentUrls, mainManifestContents);
	}

	@Override
	public boolean isEndLess() {
		return this.endLess;
	}

	@Override
	public void setEndLess(boolean endLess) {
		this.endLess = endLess;
	}

	public boolean isEmpty() {
		return (this.getBytes() == null || this.getBytes().length == 0);
	}

	@Override
	public byte[] download() throws IOException {
		if (this.getOpenedReader() == null)
			return new byte[0];

		return this.getOpenedReader().download();
	}

	public byte[] downloadAllNested() throws IOException {
		if (this.getOpenedReader() == null)
			return new byte[0];

		return this.getOpenedReader().downloadAllNested();
	}

	@Override
	public InputStream openStream() throws IOException {
		if (this.getOpenedReader() == null)
			return null;

		return this.getOpenedReader().getInputStream();
	}

	@Override
	public void closeStream() {
		if (this.getManifest() != null)
			this.getManifest().closeStream();
		if (this.getOpenedReader() != null)
			this.getOpenedReader().closeStream();
	}

	@Override
	public int compare(Object segment1, Object segment2) {
		return ((Segment) segment2).compareTo(segment1);
	}

	@Override
	public int compareTo(Object segment) {
		return (((Segment) segment).getRank() - this.getRank());
	}

	@Override
	public IPart parent() {
		return this.parentPart;
	}

	@Override
	public IPart next() {
		if (this.getManifest() == null)
			return this.nextPart;

		return this.getManifest().next();
	}

	@Override
	public IPart previous() {
		return this.previousPart;
	}
}
