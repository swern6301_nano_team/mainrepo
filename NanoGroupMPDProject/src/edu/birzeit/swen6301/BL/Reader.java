/**
 * 
 */
package edu.birzeit.swen6301.BL;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import edu.birzeit.swen6301.Util.Merge;

/**
 * @author NaNo Group
 *
 */
public class Reader implements IPart {
	private static final int BUFFER_SIZE = 4096;

	private URL URL;
	private String mimeType;
	private long contentLength;
	private String url;
	private Segment segment;
	private Manifest manifest;
	private boolean endLess;

	private HttpURLConnection httpConn;
	private URLConnection urlConn;
	private InputStream inputStream;
	private byte[] bytes;
	private IPart parentPart;
	private IPart previousPart;
	private IPart nextPart;

	public Reader() {

	}

	public Reader(String url) throws MalformedURLException {
		this.setUrl(url);
	}

	public InputStream getInputStream() {
		return this.inputStream;
	}

	@Override
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public void setParentPart(IPart parentPart) {
		this.parentPart = parentPart;
	}

	@Override
	public void setNextPart(IPart nextPart) {
		this.nextPart = nextPart;
	}

	@Override
	public void setPreviousPart(IPart previousPart) {
		this.previousPart = previousPart;
	}

	public URL getURL() {
		return URL;
	}

	public void setURL(URL uRL) {
		URL = uRL;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	@Override
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) throws MalformedURLException {
		this.url = url;
		this.setURL(new URL(this.getUrl()));
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public Manifest getManifest() {
		return manifest;
	}

	public void setManifest(Manifest manifest) {
		this.manifest = manifest;
	}

	public boolean hasManifest() {
		return ((this.getMimeType() != null && this.getMimeType().equalsIgnoreCase(Manifest.CONTENT_TYPE))
				|| (this.getUrl() != null && this.getUrl().toLowerCase().endsWith(".segments")));
	}

	@Override
	public boolean isEndLess() {
		return this.endLess;
	}

	@Override
	public void setEndLess(boolean endLess) {
		this.endLess = endLess;
	}

	public byte[] download(String url) throws IOException {
		try {
			this.setUrl(url);
			this.openStream();
		} catch (MalformedURLException e) {
			throw new IOException();
		}

		return this.download();
	}

	@Override
	public byte[] download() throws IOException {
		try {
			int bytesRead = -1;
			byte[] buffer = new byte[BUFFER_SIZE];
			bytes = new byte[0];
			while ((bytesRead = inputStream.read(buffer)) != -1)
				bytes = Merge.merge(bytes, buffer, bytesRead);
		} catch (IOException e) {
			throw new IOException();
		} catch (Exception e) {
			throw new IOException();
		} finally {

		}
		return bytes;
	}

	public byte[] downloadAllNested() throws IOException {
		if (this.hasManifest() && this.getManifest() != null)
			bytes = this.getManifest().downloadAllNested();
		else
			bytes = this.download();
		return bytes;
	}

	@Override
	public InputStream openStream() throws IOException {
		return this.openStream(false);
	}

	public InputStream openStream(boolean mainManifestContents) throws IOException {
		int responseCode;

		try {
			urlConn = this.getURL().openConnection();
			httpConn = (HttpURLConnection) urlConn;
			responseCode = httpConn.getResponseCode();
			this.setContentLength(httpConn.getContentLengthLong());

			if (responseCode == HttpURLConnection.HTTP_OK) {
				this.setMimeType(httpConn.getContentType());

				if (this.hasManifest() && !mainManifestContents) {
					inputStream = openManifestStream();
				} else
					inputStream = httpConn.getInputStream();
			} else {
				throw new IOException();
			}
		} catch (IOException e) {
			throw new IOException();
		} catch (ClassCastException e) {
			try {
				if (this.hasManifest() && !mainManifestContents) {
					inputStream = openManifestStream();
				} else
					inputStream = urlConn.getInputStream();
			} catch (IOException e1) {
				throw new IOException();
			}
		}

		return inputStream;
	}

	private InputStream openManifestStream() throws IOException {
		Manifest manifest = (Manifest) this.findParent(this);

		if (manifest == null)
			manifest = new Manifest(this.getUrl(), this.parent());
		else
			this.previous().setEndLess(true);

		manifest.setPreviousPart(this.previous());
		this.setManifest(manifest);

		return manifest.openStream();
	}

	public IPart findParent(IPart part) {
		if (part == null)
			return null;

		IPart parent = part;

		while (true) {

			if (parent != part && part.getUrl().equals(parent.getUrl()))
				return parent;
			else if (parent.parent() == null)
				return null;

			parent = parent.parent();
		}
	}

	@Override
	public void closeStream() {

	}

	@Override
	public IPart parent() {
		return this.parentPart;
	}

	@Override
	public IPart next() {
		if (this.getManifest() == null)
			return nextPart;

		return this.getManifest().next();
	}

	@Override
	public IPart previous() {
		return this.previousPart;
	}
}
