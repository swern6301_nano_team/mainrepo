/**
 * 
 */
package edu.birzeit.swen6301.BL;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import edu.birzeit.swen6301.Util.Merge;

/**
 * @author NaNo Group
 *
 */
public class Manifest implements IPart {
	public static final String CONTENT_TYPE = "text/segments-manifest";

	private IPart parentPart;
	private IPart nextPart;
	private IPart previousPart;
	private List<Segment> segments;
	private String url;
	private byte[] bytes;
	private boolean endLess;

	int rank = 0;
	String segmentStr = "";

	public Manifest() {

	}

	public Manifest(String url, IPart parent) throws IOException {
		this.setParentPart(parent);
		this.loadAndBind(url);
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}

	public boolean sortSegments() {
		if (this.getSegments() == null)
			return false;
		else
			this.getSegments().sort(new Segment(this));

		return true;
	}

	@Override
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	@Override
	public String getMimeType() {
		return CONTENT_TYPE;
	}

	public void loadAndBind(String url) throws IOException {
		System.out.println(this.load(url));
		this.bind(this.load(url));
	}

	public void bind(String manifestStr) throws IOException {
		String lines[] = manifestStr.split("\\r?\\n");
		this.bind(lines);
	}

	public void bind(String[] lines) throws IOException {
		rank = 0;
		segmentStr = "";

		for (int i = 0; i < lines.length; i++) {
			if (lines[i].trim().equals("**") && !segmentStr.trim().equals("")) {
				this.bindSegment();
			} else {
				segmentStr = segmentStr + lines[i] + System.lineSeparator();
			}
		}
		if (!segmentStr.trim().equals(""))
			this.bindSegment();
	}

	public void bindSegment() throws IOException {
		Segment segment = new Segment(this);

		try {
			segment.bind(segmentStr);
			segment.setRank(rank);
			this.addSegment(segment);

			if (rank == 0)
				segment.setPreviousPart(this.previous());
			else {
				IPart lastPart = lastOf(this.getSegments().get(rank - 1));
				if (lastPart != null) {
					segment.setPreviousPart(lastPart);
					lastPart.setNextPart(segment);
				}
			}

			segmentStr = "";
			rank++;
		} catch (IOException e) {
			System.err.println("Error in bindSegment method(): " + e);
		}
	}

	public IPart lastOf(IPart part) {
		if (part == null)
			return null;
		IPart last = part;
		IPart previous = part;

		while (true) {
			if (last.isEndLess())
				return null;
			else if (last.next() == null)
				return last;
			last = last.next();
			last.setPreviousPart(previous);
			previous = last;
		}
	}

	public String load(String url) throws IOException {
		this.setUrl(url);

		return this.load();
	}

	public String load() throws IOException {
		String contents = null;
		Segment segment = new Segment(this, this.getUrl(), true);

		if (segment.getOpenedReader() == null)
			return "";

		if (!segment.getOpenedReader().hasManifest()) {// If direct link (not manifest).
			//isn't manifest, it is direct link.
			return this.getUrl();
		}
			
		contents = new String(segment.download());
		return contents;
	}

	@Override
	public byte[] download() throws IOException {
		if (this.getSegments() == null || this.getSegments().get(0) == null)
			return new byte[0];

		return this.getSegments().get(0).download();
	}

	public byte[] downloadAllNested() throws IOException {
		bytes = new byte[0];

		if (getSegments() == null)
			return bytes; // If Empty.

		Segment segment = new Segment(this);
		this.sortSegments();

		for (int i = 0; i < getSegments().size(); i++) {
			try {
				segment = getSegments().get(i);
				bytes = Merge.merge(bytes, segment.downloadAllNested());
			} catch (IOException e) {
				System.err.println("Error in downloadAllNested Method(): " + e);
			}
		}

		return bytes;
	}

	@Override
	public boolean isEndLess() {
		return this.endLess;
	}

	@Override
	public void setEndLess(boolean endLess) {
		this.endLess = endLess;
	}

	public void addSegment(Segment segment) {
		if (segments == null) {
			segments = new ArrayList<Segment>();
		}
		segments.add(segment);
	}

	@Override
	public void setPreviousPart(IPart previousPart) {
		this.previousPart = previousPart;
	}

	public void setParentPart(IPart parentPart) {
		this.parentPart = parentPart;
	}

	@Override
	public void setNextPart(IPart nextPart) {
		this.nextPart = nextPart;
	}

	@Override
	public InputStream openStream() throws IOException {
		if (this.getSegments() == null || this.getSegments().get(0) == null)
			return null;

		return this.getSegments().get(0).openStream();
	}

	@Override
	public void closeStream() {
		if (getSegments() == null)
			return;
		for (int i = 0; i < getSegments().size(); i++) {
			getSegments().get(i).closeStream();
		}
	}

	@Override
	public IPart parent() {
		return this.parentPart;
	}

	@Override
	public IPart next() {
		if (this.getSegments() == null)
			return null;

		try {
			return this.getSegments().get(1);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public IPart previous() {
		return this.previousPart;
	}
}
