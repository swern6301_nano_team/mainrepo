/**
 * 
 */
package edu.birzeit.swen6301.BL;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author NaNo Group
 *
 */
public interface IPart {

	public String getUrl();

	public String getMimeType();

	public InputStream openStream() throws IOException;

	public void closeStream();

	public byte[] download() throws IOException;

	public void setNextPart(IPart nextPart);

	public void setPreviousPart(IPart previousPart);

	public boolean isEndLess();

	public void setEndLess(boolean endLess);

	public IPart parent();

	public IPart next();

	public IPart previous();
}
