/**
 * 
 */
package edu.birzeit.swen6301.Util;

import java.io.IOException;
import java.io.InputStream;
import edu.birzeit.swen6301.BL.IPart;
import edu.birzeit.swen6301.BL.Manifest;

/**
 * @author NaNo Group
 *
 */
public class MultiPartStream extends InputStream {

	private InputStream inputStream;
	private IPart part;
	private IPart firstPart;

	public MultiPartStream(IPart firstPart) {
		this.firstPart = firstPart;
	}

	public MultiPartStream(String url) throws IOException {
		this.firstPart = new Manifest(url, null);
	}

	public IPart nextPart() {
		if (this.part == null)
			this.part = this.firstPart;
		else
			this.part = this.part.next();

		return this.part;
	}

	public InputStream openStream() {
		return this;
	}

	@Override
	public int read() throws IOException {
		byte[] bytes = new byte[1];
		this.read(bytes);

		return Byte.toUnsignedInt(bytes[0]);
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		int bytesRead = -1;

		try {
			if (inputStream == null) { // End of part.
				if (this.nextPart() != null) {
					if (this.part.openStream() == null) {
						buffer = new byte[0];
						return 0;
					} else {
						inputStream = this.part.openStream();
					}
				} else {
					this.part = null;
					return -1; // EOF
				}
			}

			if ((bytesRead = inputStream.read(buffer)) == -1) {
				inputStream = null;
				return this.read(buffer);
			}
		} catch (IOException e) {
			throw new IOException(e.getMessage());
		}
		return bytesRead;
	}

	@Override
	public void close() {
		if (this.firstPart != null)
			this.firstPart.closeStream();
	}
}
