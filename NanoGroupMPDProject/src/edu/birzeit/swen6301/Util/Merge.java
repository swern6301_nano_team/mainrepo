/**
 * 
 */
package edu.birzeit.swen6301.Util;

/**
 * @author NaNo Group
 *
 */
public class Merge {
	public static byte[] merge(byte[] a, byte[] b) {
		return merge(a, b, 0, b.length);
	}

	public static byte[] merge(byte[] a, byte[] b, int bLength) {
		return merge(a, b, 0, bLength);
	}

	public static byte[] merge(byte[] a, byte[] b, int bStart, int bLength) {
		byte[] tmp = new byte[a.length + bLength];
		System.arraycopy(a, 0, tmp, 0, a.length);
		System.arraycopy(b, bStart, tmp, a.length, bLength);
		a = tmp.clone();
		return a;
	}
}