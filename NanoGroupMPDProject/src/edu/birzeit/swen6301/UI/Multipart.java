/**
 * 
 */
package edu.birzeit.swen6301.UI;

import java.io.IOException;
import java.io.InputStream;
import edu.birzeit.swen6301.Util.MultiPartStream;

/**
 * @author NaNo Group
 *
 */
public class Multipart {
	public static InputStream openStream(String url) throws IOException {
		MultiPartStream multiPartStream = new MultiPartStream(url);
		return multiPartStream;
	}
}